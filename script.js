
// Выполнить проверку полей по нажатию на «Заказать». Активный чекбокс соглашения обязательный параметр формы.

//Рассчитать общую сумму заказа
 function calculateSumOrder(){

        //Меняем общую сумму заказа в добавленных позициях
        var sumOfKomplekts = $(document).find("[sum-price-element]");
        //Заново рассчитываем сумму заказа
        var sumOfOrder = 0;
        

        //Собираем суммы всех заказов
        for(var i = 0; i<sumOfKomplekts.length; i++){

            let sumItemNumber =  $(sumOfKomplekts[i]).text();
            let sumItem = numberWithSpaces(sumItemNumber, true);
            sumOfOrder += sumItem;

        }

        //Рассчитываем общую новую сумму заказа
        sumOfOrder = numberWithSpaces(sumOfOrder, false)
        $(document).find("[sum-orders]").text(sumOfOrder);

    }

//Разделение числа на разряды и Соединение числа с разрядами
function numberWithSpaces(x, reverseAction) {
  return reverseAction ? +x.replace(" ", "") : x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

//Удалить комплект - скрывает весь комплект на странице
function hideKomplekt(){
    console.log("Элемент «удалить комплект» скрывает весь комплект на странице");
     
    var elemAttrName= $(this).attr("data-icon-element");
    var containerKomplekt = $("div").find(`[data-target-element='${elemAttrName}']`);
    var containerPrice =  $(document).find(`[sum-price-element='${elemAttrName}']`).text();
    var priceNumber = +containerPrice.replace(" ", "");
    var sumBasketContainer = $(document).find(`[sum-orders='all']`).text();
    var sumOrder = numberWithSpaces(sumBasketContainer, true);

    //Пересчет итоговой цены заказа
    if(sumOrder > 0){
        let newSumOrder = sumOrder - priceNumber;
        //Возращает сумму в формат строки с пробелами
        let newSumOrderString = numberWithSpaces(newSumOrder, false);
        $(document).find(`[sum-orders='all']`).text(newSumOrderString)
        
    }

    //Скрываем позицию в блоке "ДОБАВЛЕННЫЕ ПОЗИЦИИ"
    $(".added_orders_list").find(`[data-basket-item-komplekt='${elemAttrName}']`).hide();
    //Скрываем сам комплект
    containerKomplekt.hide();

    

}

//Элемент «удалить» в строке с товаром скрывает только товар и пересчитывает корзину
function hidePosition(){
    console.log("Элемент «удалить» в строке с товаром скрывает только товар и пересчитывает корзину");
   
    //Получаем контенер цены
    var basketItemParentContainer =  $(this).parent().parent();
    var basketItemPriceContainer = basketItemParentContainer.find(".basket-item-price-number");

    //Получаем цену 
    var basketItemPrice = basketItemPriceContainer.text();
    var basketItemPriceNumber = numberWithSpaces(basketItemPrice, true);

    //Получаем количество
    var basketItemQuantity = ($(this).parent().siblings(".basket-item-quantity").find("input.number").attr("value"));
    //Расчет позиции
    var basketItemSum = +basketItemQuantity * +basketItemPriceNumber;

    //Получаем итоговую цену комплекта
    var typeKomplekt = basketItemPriceContainer.attr("data-komplekt-element");
    var komplektPriceNumber =  $(document).find(`[sum-price-element='${typeKomplekt}']`).text();
    var komplektPrice = numberWithSpaces(komplektPriceNumber, true);

    //Пересчет итоговой цены комплекта
     if(komplektPrice > 0){
        let komplektSum = komplektPrice - basketItemSum;
        let komplektSumString = numberWithSpaces(komplektSum, false);
        $(document).find(`[sum-price-element='${typeKomplekt}']`).text(komplektSumString)
    }

    //Скрываем саму позицию
     basketItemParentContainer.hide();

     //Если позиций товара в группе больше нет то скрываем и саму группу товара КОСТЫЛЬ
     var basketItemGroup = basketItemParentContainer.prev(".basket-item-group");
     basketItemGroup.hide()

    //Пересчитываем стоимость корзину
     calculateSumOrder();
     

}

//Изменение количества пересчитывает корзину
function calculateSum(){
    console.log("Изменение количества пересчитывает корзину");

    //Получаем аттрибут
    var quantity = parseInt($(this).siblings(".basket-item-amount-filed-block").find(".number").attr("value"));

    if($(this).attr("typesum") == "minus"){

        if(quantity > 1){
            $(this).next().find(".number").attr("value", quantity -= 1);
        }
    }
    else{

        $(this).prev().find(".number").attr("value", quantity += 1);
        quantity = $(this).prev().find(".number").attr("value");
    }

    var basketSum = 0;
    var basketItemType = $(this).parent().parent().parent().find(".basket-item-price-number").attr("data-komplekt-element");


    var basketItemContainer = $(document).find(`[data-target-element=${basketItemType}]`);

    var quantityBlocks =  basketItemContainer.find("input.number");
    var priceBlocks = basketItemContainer.find("span.basket-item-price-number");

    for(var i = 0; i < quantityBlocks.length; i++){

        let priceItemNumber =  $(priceBlocks[i]).text();
        let priceItem = numberWithSpaces(priceItemNumber, true);;
        
        let quantityItem =  $(quantityBlocks[i]).attr("value");
        basketSum += +quantityItem * priceItem;

    }


    var basketSumString = numberWithSpaces(basketSum, false);
    var sumContainer = $(document).find(`[sum-price-element=${basketItemType}]`).text(basketSumString);

    //Пересчитываем стоимость корзину
    calculateSumOrder();
   
   
}

//Элемент «Очистить все позиции» скрывает полностью все товары со страницы
function cleanPositions(){

    console.log("Элемент «Очистить все позиции» скрывает полностью все товары со страницы");
    //Получаем все комплекты
    var basketItems = $(document).find(".item");

    for(var i = 0; i < basketItems.length; i++){

        $(basketItems[i]).hide();
        var basketItemAttr = $(basketItems[i]).attr("data-target-element");
       
              
    }

    $(document).find(".added_orders_item").hide();
    //Обнуляем сумму
    $(document).find("[sum-orders]").text(0);






}

function checkFields(){
    console.log("Выполнить проверку полей по нажатию на «Заказать». Активный чекбокс соглашения обязательный параметр формы");
   

}




//Назначение обработчиков

var titleControls = document.getElementsByClassName("delete-kit");

for (var i = 0; i < titleControls.length; i++) {
    titleControls[i].addEventListener("click", hideKomplekt, false);
}

var basketControls = document.getElementsByClassName("delete-basket-item");

for (var i = 0; i < basketControls.length; i++) {
    basketControls[i].addEventListener("click", hidePosition, false);
}

var quantityControls = document.getElementsByClassName("basket-amount-change");

for (var i = 0; i < quantityControls.length; i++) {
    quantityControls[i].addEventListener("click", calculateSum, false);
}

var addedOrderControl = document.getElementsByClassName("added_orders_delete-control");

for (var i = 0; i < addedOrderControl.length; i++) {
    addedOrderControl[i].addEventListener("click", cleanPositions, false);
}

var footerFormSubmit = document.getElementsByClassName("footer-form-submit");

for (var i = 0; i < footerFormSubmit.length; i++) {
    footerFormSubmit[i].addEventListener("click", checkFields, false);
}









